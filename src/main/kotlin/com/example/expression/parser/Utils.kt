package com.example.expression.parser

import java.util.regex.Matcher
import java.util.regex.Pattern

fun String.splitByPattern(pattern: Pattern): Sequence<String> = splitWithMatcher(pattern.matcher(this))

private fun String.splitWithMatcher(matcher: Matcher, from: Int = 0): Sequence<String> {
    return if (matcher.find()) {
        sequence {
            if (matcher.start() - from > 0) {
                yield(substring(from, matcher.start()))
            }
            yield(substring(matcher.start(), matcher.end()))
            yieldAll(splitWithMatcher(matcher, matcher.end()))
        }
    } else {
        if (length - from > 0) sequenceOf(substring(from, length)) else emptySequence()
    }
}