package com.example.expression.parser.component

import java.math.BigDecimal

sealed class Func {
    abstract fun calculate(variable: BigDecimal): BigDecimal
    abstract fun map(mapper: (BigDecimal) -> BigDecimal): Func
    abstract fun flatMap(mapper: (BigDecimal) -> Func): Func
    fun combine(other: Func, combinator: (BigDecimal, BigDecimal) -> BigDecimal)
        = flatMap { target -> other.map { otherValue -> combinator(target, otherValue) } }

    class Constant(val const: BigDecimal): Func() {
        override fun calculate(variable: BigDecimal) = const
        override fun map(mapper: (BigDecimal) -> BigDecimal) = try {
            Constant(mapper(const))
        } catch (e: Throwable) {
            Error(e)
        }
        override fun flatMap(mapper: (BigDecimal) -> Func) = mapper(const)
        override fun equals(other: Any?) = other != null && other is Constant && other.const == const
        override fun hashCode() = const.hashCode()
    }

    class Variable(private val expression: (BigDecimal) -> BigDecimal): Func() {
        companion object {
            val baseVariable: Variable
                get() = Variable { it }
        }
        override fun calculate(variable: BigDecimal) = expression(variable)
        override fun map(mapper: (BigDecimal) -> BigDecimal) = Variable { mapper(expression(it)) }
        override fun flatMap(mapper: (BigDecimal) -> Func) = Variable { mapper(expression(it)).calculate(it) }
    }

    class Error(val exception: Throwable): Func() {
        override fun calculate(variable: BigDecimal) = throw RuntimeException(exception)
        override fun map(mapper: (BigDecimal) -> BigDecimal) = this
        override fun flatMap(mapper: (BigDecimal) -> Func) = this
    }
}