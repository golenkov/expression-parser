package com.example.expression.parser.component

import ch.obermuhlner.math.big.BigDecimalMath
import com.example.expression.parser.exception.ParsingException
import com.example.expression.parser.splitByPattern
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.math.MathContext

@Component
class ExpressionParser {
    companion object {
        val pattern = """([+*()^/-]|sqrt)""".toPattern()
        private const val variableLiteral = "x"
        private val stringToTokenGeneratorMap: Map<String, Token> = (Operation.operations.map { it.value to it }
        + listOf("(" to OpeningParenthesis, ")" to ClosingParenthesis)).toMap()
    }

    fun parse(s: String): Func {
        val tokensList: List<Token> = s.toLowerCase().splitByPattern(pattern).map(this::mapToToken).toList()
        return processExpressionTokens(tokensList).func
    }

    protected fun processExpressionTokens(tokenList: List<Token>): FuncPart = processOperations(
        processParentheses(tokenList)
    )

    protected fun processOperations(tokenList: List<Token>): FuncPart {
        return Operation.operations.sortedByDescending { it.priority }.fold(tokenList) { tokens, operation ->
            tokens.withIndex().filter { it.value == operation }.map { it.index }.let { applyOperation(operation, it, tokens) }
        }.singleOrNull() as? FuncPart
            ?: throw ParsingException()
    }

    private fun applyOperation(operation: Operation, operationIndexes: List<Int>, tokenList: List<Token>): List<Token> {
        if (operationIndexes.isEmpty()) {
            return tokenList
        }

        val newTokenList: MutableList<Token> = mutableListOf()
        var lastProcessed: Int = -1
        operationIndexes.forEach { index ->
            if (index > lastProcessed) {
                if (index == tokenList.size - 1) {
                    throw ParsingException()
                }
                var rightOperandIndex = index + 1
                val rightUnaryOperations = mutableListOf<Operation.UnaryOperation>()

                while (tokenList[rightOperandIndex] is Operation.UnaryOperation && rightOperandIndex < tokenList.size - 1 ) {
                    rightUnaryOperations+=tokenList[rightOperandIndex] as Operation.UnaryOperation
                    rightOperandIndex++
                }
                if (tokenList[rightOperandIndex] !is FuncPart) {
                    throw ParsingException()
                }
                val rightOperand = rightUnaryOperations
                    .foldRight(tokenList[rightOperandIndex] as FuncPart) { op, funcPart -> op.unaryAction(funcPart) }

                val leftOperandIndex = index - 1

                if (operation is Operation.BinaryOperation && leftOperandIndex >= 0 && tokenList[leftOperandIndex] is FuncPart) {
                    val res = operation.binaryAction(tokenList[leftOperandIndex] as FuncPart, rightOperand)
                    newTokenList.addAll(tokenList.subList(lastProcessed + 1, leftOperandIndex))
                    newTokenList.add(res)
                } else if (operation is Operation.UnaryOperation) {
                    val res = operation.unaryAction(rightOperand)
                    newTokenList.addAll(tokenList.subList(lastProcessed + 1, index))
                    newTokenList.add(res)
                } else {
                    throw ParsingException()
                }
                lastProcessed = rightOperandIndex
            }
        }
        if (lastProcessed + 1 < tokenList.size) {
            newTokenList.addAll(tokenList.subList(lastProcessed + 1, tokenList.size))
        }

        return newTokenList
    }

    protected fun processParentheses(tokensList: List<Token>): List<Token> {
        val newList: MutableList<Token> = mutableListOf()
        var subExpressionFrom = 0
        var parenthesesCounter = 0
        tokensList.forEachIndexed { index, token ->
            if (parenthesesCounter > 0) {
                when (token) {
                    OpeningParenthesis -> parenthesesCounter++
                    ClosingParenthesis -> {
                        if (--parenthesesCounter == 0) {
                            newList+= processExpressionTokens(tokensList.subList(subExpressionFrom+1, index))
                        }
                    }
                }
            } else {
                when (token) {
                    ClosingParenthesis -> throw ParsingException()
                    OpeningParenthesis -> {
                        parenthesesCounter++
                        subExpressionFrom = index
                    }
                    else -> newList+= token
                }
            }
        }
        if (parenthesesCounter > 0) throw ParsingException()

        return newList
    }

    protected fun mapToToken(str: String): Token {
        return str.trim().let { token ->
            stringToTokenGeneratorMap[token]
                ?: token.toBigDecimalOrNull()?.let { FuncPart(Func.Constant(it)) }
                ?: if (token == variableLiteral) FuncPart(Func.Variable.baseVariable)
                else throw ParsingException()
        }
    }
}

val mathContext = MathContext(10)

sealed class Token

sealed class Operation(val priority: Int, val value: String): Token() {
    companion object {
        val operations = Operation::class.nestedClasses.mapNotNull { it.objectInstance as? Operation }
    }

    interface UnaryOperation { fun unaryAction(funcPart: FuncPart): FuncPart }
    interface BinaryOperation { fun binaryAction(first: FuncPart, second: FuncPart): FuncPart }

    object Addition: Operation(priority = 0, value = "+"), BinaryOperation {
        override fun binaryAction(first: FuncPart, second: FuncPart) = first.combine(second, BigDecimal::plus)
    }
    object Subtraction: Operation(priority = 0, value = "-"), BinaryOperation, UnaryOperation {
        override fun unaryAction(funcPart: FuncPart) = FuncPart(funcPart.func.map { -it })
        override fun binaryAction(first: FuncPart, second: FuncPart) = first.combine(second, BigDecimal::minus)
    }
    object Multiplication: Operation(priority = 1, value = "*"), BinaryOperation {
        override fun binaryAction(first: FuncPart, second: FuncPart) = first.combine(second, BigDecimal::multiply)
    }
    object Division: Operation(priority = 1, value = "/"), BinaryOperation {
        override fun binaryAction(first: FuncPart, second: FuncPart) = first.combine(second) { f, s -> f.divide(s, mathContext) }
    }
    object Exponentiation: Operation(priority = 2, value = "^"), BinaryOperation {
        override fun binaryAction(first: FuncPart, second: FuncPart) = first
            .combine(second) { one, other -> BigDecimalMath.pow(one, other, mathContext) }
    }
    object SquareRoot: Operation(priority = 2, value = "sqrt"), UnaryOperation {
        override fun unaryAction(funcPart: FuncPart) = FuncPart(funcPart.func.map { it.sqrt(mathContext) })
    }
}
object OpeningParenthesis: Token()
object ClosingParenthesis: Token()
class FuncPart(val func: Func): Token() {
    fun combine(other: FuncPart, combinator: (BigDecimal, BigDecimal) -> BigDecimal) = FuncPart(func.combine(other.func, combinator))
    override fun equals(other: Any?) = other != null && other is FuncPart && other.func == func
    override fun hashCode() = func.hashCode()
}
