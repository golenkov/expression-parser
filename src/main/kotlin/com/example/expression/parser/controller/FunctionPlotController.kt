package com.example.expression.parser.controller

import com.example.expression.parser.service.FunctionPlotService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO

@Controller
class FunctionPlotController(private val functionPlotService: FunctionPlotService) {
    @GetMapping(path = ["/function.jpg"], produces = ["image/jpg"])
    fun plotFunction(
        @RequestParam expression: String,
        @RequestParam from: Double,
        @RequestParam to: Double
    ): ResponseEntity<ByteArray> {
        return ResponseEntity(
            ByteArrayOutputStream()
                .also {
                    ImageIO.write(
                        functionPlotService.plot(expression, from.toBigDecimal(), to.toBigDecimal()),
                        "jpg",
                        it
                    )
                }.toByteArray(),
            HttpStatus.OK
        )
    }
}