package com.example.expression.parser.service

import com.example.expression.parser.component.ExpressionParser
import com.example.expression.parser.component.Func
import com.example.expression.parser.component.mathContext
import com.example.expression.parser.exception.RangeException
import org.springframework.stereotype.Service
import java.awt.Color
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.geom.Line2D
import java.awt.image.BufferedImage
import java.math.BigDecimal

@Service
class FunctionPlotService(private val expressionParser: ExpressionParser) {

    private val defaultWidth = 600
    private val defaultHeight = 600

    fun plot(expression: String, from: BigDecimal, to: BigDecimal): BufferedImage {
        if (to <= from) {
            throw RangeException()
        }
        val image = BufferedImage(defaultWidth, defaultHeight, BufferedImage.TYPE_INT_RGB)
        val parsedFun = expressionParser.parse(expression)
        val points: List<Pair<BigDecimal, BigDecimal>> = getPoints(parsedFun, from, to, getStep(from, to, image.width))

        if (points.size < 2) {
            return image
        }

        val scaleCoefficient = maxOf(
            points.map{ it.first.abs() }.max()!!,
            points.map { it.second.abs() }.max()!!
        )
            .divide(((minOf(image.width, image.height)/2)).toBigDecimal(), mathContext)
            .let { minOf(it, 1.toBigDecimal()) }

        val graphics = image.createGraphics()
        preparePlot(graphics, image.width, image.height)
        graphics.color = Color.red
        points
            .map { (x, y) -> x.divide(scaleCoefficient, mathContext) to y.divide(scaleCoefficient, mathContext) }
            .zipWithNext()
            .forEach { drawFunctionPart(graphics, it.first, it.second, image.width, image.height) }
        graphics.dispose()

        return image
    }

    private fun drawFunctionPart(
        graphics: Graphics2D,
        from: Pair<BigDecimal, BigDecimal>,
        to: Pair<BigDecimal, BigDecimal>,
        plotWidth: Int,
        plotHeight: Int
    ) {
        graphics.draw(
            Line2D.Double(
                normX(from.first, plotWidth).toDouble(),
                normY(from.second, plotHeight).toDouble(),
                normX(to.first, plotWidth).toDouble(),
                normY(to.second, plotHeight).toDouble()
            )
        )
    }

    private fun normX(x: BigDecimal, width: Int) = x + (width / 2).toBigDecimal()

    private fun normY(y: BigDecimal, height: Int) = -y + (height / 2).toBigDecimal()

    private fun getPoints(func: Func, from: BigDecimal, to: BigDecimal, step: BigDecimal): List<Pair<BigDecimal, BigDecimal>> {
        return sequence {
            var currentX = from
            while (currentX <= to) {
                yield(
                    try {
                        currentX to func.calculate(currentX)
                    } catch (e: Throwable) {
                        null
                    }
                )
                currentX = currentX + step
            }
        }.filterNotNull().toList()
    }

    private fun getStep(from: BigDecimal, to: BigDecimal, xSize: Int) = (to - from).divide(xSize.toBigDecimal(), mathContext)

    private fun preparePlot(graphics: Graphics, width: Int, height: Int) {
        val color = graphics.color
        graphics.color = Color.white
        graphics.fillRect(0, 0, width, height)
        graphics.color = Color.black
        graphics.drawLine(0, height/2, width, height/2)
        graphics.drawLine(width/2, 0, width/2, height)

        graphics.color = color
    }
}