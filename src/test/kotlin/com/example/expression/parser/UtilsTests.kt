package com.example.expression.parser

import com.example.expression.parser.component.ExpressionParser
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*


@ExtendWith(MockitoExtension::class)
class UtilsTests {
    @Test
    fun whenSplitMathExpressionToTokensWithRightPatternThenSuccess() {
        val expected = listOf(
            "4",
            "/",
            "sqrt",
            "(",
            "16",
            ")",
            "+",
            "1",
            "-",
            "5",
            "^",
            "3",
            "*",
            "(",
            "4",
            "-",
            "1",
            ")"
        )
        assertThat("4/sqrt(16)+1-5^3*(4-1)".splitByPattern(ExpressionParser.pattern).toList(), equalTo(expected))
    }

    @Test
    fun whenSplitStringWithWrongTokenThenReturnOnlyOneSubstringEqualGiven() {
        val string = "rundomstring"
        string.splitByPattern("(wrongPattern)".toPattern()).toList().also {
            assertThat(it.size, equalTo(1))
            assertThat(it, hasItem(string))
        }
    }
}