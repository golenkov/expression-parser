package com.example.expression.parser.component

import com.example.expression.parser.exception.ParsingException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class ExpressionParserMapToTokenTests {

    private val expressionParser = TestExpressionParser()

    @Test
    fun whenRandomStringThrowsException() {
        assertThrows<ParsingException> {
            expressionParser.callMapToToken("randomString")
        }
    }

    @Test
    fun whenXThenVariable() {
        expressionParser.callMapToToken("x").let {
            assert(it is FuncPart && it.func is Func.Variable)
        }
    }

    @Test
    fun whenPlusThenAddition() {
        expressionParser.callMapToToken("+").let {
            assert(it is Operation.Addition)
        }
    }

    @Test
    fun whenMinusThenSubtraction() {
        expressionParser.callMapToToken("-").let {
            assert(it is Operation.Subtraction)
        }
    }

    @Test
    fun `when*Then`() {
        expressionParser.callMapToToken("*").let {
            assert(it is Operation.Multiplication)
        }
    }

    @Test
    fun whenSlashThenConst() {
        expressionParser.callMapToToken("/").let {
            assert(it is Operation.Division)
        }
    }

    @Test
    fun whenSqrtThenSquareRoot() {
        expressionParser.callMapToToken("sqrt").let {
            assert(it is Operation.SquareRoot)
        }
    }

    @Test
    fun `when^ThenExponentiation`() {
        expressionParser.callMapToToken("^").let {
            assert(it is Operation.Exponentiation)
        }
    }

    private class TestExpressionParser: ExpressionParser() {
        fun callMapToToken(token: String) = mapToToken(token)
    }
}
