package com.example.expression.parser.component

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class ExpressionParserParseTests {

    private val expressionParser = ExpressionParser()

    @Test
    fun whenConstExpressionThenSuccessCalculate() {
        expressionParser.parse("4/sqrt(16)+1-5^2*(4-1)").let {
            assert(it is Func.Constant && it.const == -(73).toBigDecimal())
        }
    }

    @Test
    fun whenExpressionWithVariableThenSuccessBuildFunction() {
        expressionParser.parse("4/sqrt(16)+1-x^2*(4-1)").let {
            assert(it is Func.Variable && it.calculate(5.toBigDecimal()) == -(73).toBigDecimal())
        }
    }

}
