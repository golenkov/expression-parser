package com.example.expression.parser.component

import org.hamcrest.MatcherAssert.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.hamcrest.Matchers.*

@ExtendWith(MockitoExtension::class)
class ExpressionParserProcessParenthesesTests {

    private val expressionParser = TestExpressionParser()

    @AfterEach
    fun afterEach() {
        expressionParser.processExpressionTokensCalls.clear()
    }

    @Test
    fun whenExpressionContainParenthesesCallingProcessExpressionTokensWithSubexpression() {
        val tokens = listOf (
            FuncPart(Func.Constant(2.toBigDecimal())),
            Operation.Addition,
            OpeningParenthesis,
            FuncPart(Func.Constant(2.toBigDecimal())),
            Operation.Addition,
            FuncPart(Func.Constant(2.toBigDecimal())),
            ClosingParenthesis
        )
        val result = expressionParser.callProcessParentheses(tokens)

        assertThat(result, equalTo(listOf(
            FuncPart(Func.Constant(2.toBigDecimal())),
            Operation.Addition,
            TestExpressionParser.processExpressionTokensResponse
        )))

        assertThat(expressionParser.processExpressionTokensCalls, equalTo(listOf(listOf(
            FuncPart(Func.Constant(2.toBigDecimal())),
            Operation.Addition,
            FuncPart(Func.Constant(2.toBigDecimal()))
        ))))
    }

    @Test
    fun whenExpressionDoesntContainParenthesesDontCallingProcessExpressionTokensAndReturnExpression() {
        val tokens = listOf (
            FuncPart(Func.Constant(2.toBigDecimal())),
            Operation.Addition,
            FuncPart(Func.Constant(2.toBigDecimal())),
            Operation.Addition,
            FuncPart(Func.Constant(2.toBigDecimal()))
        )
        val result = expressionParser.callProcessParentheses(tokens)

        assertThat(result, equalTo(tokens))

        assert(expressionParser.processExpressionTokensCalls.isEmpty())
    }

    private class TestExpressionParser: ExpressionParser() {
        companion object {
            val processExpressionTokensResponse = FuncPart(Func.Constant(1.toBigDecimal()))
        }
        val processExpressionTokensCalls = mutableListOf<List<Token>>()
        override fun processExpressionTokens(tokenList: List<Token>): FuncPart {
            processExpressionTokensCalls+=tokenList
            return processExpressionTokensResponse
        }
        fun callProcessParentheses(tokensList: List<Token>) = processParentheses(tokensList)
    }
}
